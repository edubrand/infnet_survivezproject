using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UiController : MonoBehaviour
{
    public int zCount;
    public int hCount;

    public TextMeshProUGUI zombieCount;
    public TextMeshProUGUI survivorCount;


    void Update()
    {
        survivorCount.text = "Survivor:  "+hCount.ToString();
        zombieCount.text = "Zombie:  "+zCount.ToString();
    }
}

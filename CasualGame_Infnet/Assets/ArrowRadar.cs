using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ArrowRadar : MonoBehaviour
{
    public GameObject target;
    public Vector3 LookAtPoint;
    public Quaternion _playerRot;

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //transform.LookAt(target.transform.position*-1f);
        //transform.Rotate(target.transform.position);

        transform.LookAt(target.transform.position);
        LookAtPoint = new Vector3(target.transform.position.x - transform.position.x, transform.position.y, target.transform.position.z - transform.position.z);
        LookAtPoint = LookAtPoint.normalized;
        _playerRot = Quaternion.LookRotation(LookAtPoint);
    }
}

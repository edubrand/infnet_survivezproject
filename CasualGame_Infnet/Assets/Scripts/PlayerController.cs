using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SocialPlatforms.Impl;
using TMPro;
using UnityEngine.SceneManagement;
namespace Adami
{
    public class PlayerController : MonoBehaviour
    {
        #region PlayerStats
        [HideInInspector] public Vector3 targetPosition;
        // public Vector3 LookAtPoint;
        // public Quaternion _playerRot;
        [Header("Player Stats")]
        public LayerMask clickableLayer;
        public NavMeshAgent agent;
        public Rigidbody rb;
        public Animator animPlayer;
        public AudioSource helicopter;
        public AudioSource healAudio;
        public float speed;
        public float rotSpeed;
        public int playerHP;
        public float score;
        public bool infected;
        #endregion
        #region UI
        [Header("UI")]
        public TextMeshProUGUI hpTxt;
        public TextMeshProUGUI scoreTxt;
        public TextMeshProUGUI endScoreTxt;
        public GameObject deathEffectMask;
        public GameObject endGamePopUp;
        private GameObject _uiController;
        #endregion
        [Header("Humans")]
        public List<GameObject> _humans = new List<GameObject>();
        

        void Start()
        {
            rb = GetComponent<Rigidbody>();
            _uiController = GameObject.Find("UI");
            hpTxt.text = playerHP.ToString();
            scoreTxt.text = score.ToString("0");
        }
        void Update()
        {
             if(agent.velocity.magnitude > 0.15f)
            {
            score += Time.deltaTime;
            }
            scoreTxt.text = score.ToString("0");
            if (Input.GetMouseButton(0))
            {
                Direction();

            }
        }

        public void Direction()
        {
            animPlayer.SetTrigger("Start");

            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000, clickableLayer.value))
            {
                targetPosition = hit.point;
                agent.SetDestination(targetPosition);

                //transform.LookAt(targetPosition);
                //LookAtPoint = new Vector3(targetPosition.x - transform.position.x, transform.position.y, targetPosition.z - transform.position.z);
                //LookAtPoint = LookAtPoint.normalized;
                //_playerRot = Quaternion.LookRotation(LookAtPoint);
            }
        }
        /*public void Movement()
        {
            //transform.rotation = Quaternion.Slerp(transform.rotation, _playerRot, rotSpeed * Time.deltaTime);
            //transform.Translate(Vector3.forward * speed * Time.deltaTime);
            //rb.velocity = new Vector3(LookAtPoint.x * speed, rb.velocity.y, LookAtPoint.z * speed);
            //transform.position = Vector3.MoveTowards(transform.position,targetPosition,speed*Time.deltaTime);
        }*/
        public void Rescue()
        {
            foreach (GameObject h in _humans)
            {
                helicopter.Play();

                Destroy(h);
                ScoreCount(10);
                playerHP--;
                _uiController.GetComponent<UiController>().hCount--;
                UpdateUI();

                //_humans.Remove(h);
            }
            _humans.Clear();
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "SafeZone")
            {
                Rescue();
            }
        }
        public void UpdateUI()
        {
            hpTxt.text = playerHP.ToString();
        }
        public void ScoreCount(float value)
        {
            score += value;
        }
        public void Damage()
        {
            if (playerHP <= 0)
            {
                infected = true;
                if (deathRoutine == null)
                {
                    deathRoutine = StartCoroutine(DeathRoutine());
                }
                
                //Destroy(this.gameObject);
                // endGamePopUp.SetActive(true);
                //endScoreTxt.text = "Score: "+ score.ToString("0"); 
                //Time.timeScale = 0f;
            }
            else
            {
                playerHP--;
                UpdateUI();
                if (_humans.Count <= 0) return;
                GameObject humanToDestroy = _humans[0];
                _humans.RemoveAt(0);
                if (humanToDestroy == null) return;
                humanToDestroy.GetComponent<HumanController>().ZombieTransformation();
            }
        }
        Coroutine deathRoutine;
        private IEnumerator DeathRoutine()
        {
            RectTransform uiEffectTransform = (RectTransform)deathEffectMask.transform;
            while (infected && uiEffectTransform.rect.width > 850)
            {
                float a = uiEffectTransform.rect.width;  // start
                float b = 800;  // end
                int x = 7;  // time frame
                float n = 0;  // lerped value
                for (float f = 0; f <= x; f += Time.deltaTime)
                {
                    n = Mathf.Lerp(a, b, f / x); // passing in the start + end values, and using our elapsed time 'f' as a portion of the total time 'x'
                    Vector2 newPos = new Vector2(n, n);
                    uiEffectTransform.sizeDelta = newPos;
                    yield return null;
                }
            }
            deathRoutine = null;
            Destroy(this.gameObject);
            endGamePopUp.SetActive(true);
            endScoreTxt.text = score.ToString("0");
            Time.timeScale = 0f;
        }
        public void HealInfection()
        {
            if (infected)
            {
                healAudio.Play();
                infected = false;
                StopCoroutine(deathRoutine);
                deathRoutine = null;
            }
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Audio;
using Adami;
public class Enemy_AI : MonoBehaviour
{
    public float movementSpeed;
    public NavMeshAgent agent;
    //private Rigidbody rigidBody;
    public Adami.PlayerController playerController;
    public Adami.HumanController humanController;
    public bool isHumanFound;
    private bool activeAI = true;
    public float range;
    private GameObject _uiController;
    public AudioSource zombieAttack;

    public List<GameObject> aggroList = new List<GameObject>();

    
    private void Awake() {
        //rigidBody = GetComponent<Rigidbody>();
        
    }
    private void Start()
    {
        _uiController = GameObject.Find("UI");
        _uiController.GetComponent<UiController>().zCount++;
    }
    
    private void Update() {

        if (activeAI)
        {
            // NavMeshPath navMeshPath = new NavMeshPath();
            // if (!isHumanFound && agent.CalculatePath(CalculateNewPosition(),navMeshPath) && navMeshPath.status == NavMeshPathStatus.PathComplete)
            // {
            //     agent.SetPath(navMeshPath);
            // } 
            Vector3 point;
            if (!isHumanFound)
            {
                if (RandomPoint(transform.position, range, out point))
                {
                    agent.SetDestination(point);
                }
            }

            else
            {
                if (playerController != null)
                {
                    agent.SetDestination(playerController.transform.position);
                }
                 if (humanController != null)
                {
                    agent.SetDestination(humanController.transform.position);
                }
            }
        }
        if(aggroList.Count == 0)
        {
            isHumanFound = false;
        }
    }
    
    private Vector3 CalculateNewPosition()
    {
        Vector2 axisPosition = Random.insideUnitCircle*5;
       // Vector3 newPosition = Random.insideUnitSphere;
        Vector3 newPosition = new Vector3(axisPosition.x,transform.position.y,axisPosition.y);
        return newPosition;
    }
    private void OnTriggerEnter(Collider other) {
        if (other.GetComponent<Adami.PlayerController>() != null)
        {
            isHumanFound = true;
            playerController = other.GetComponent<Adami.PlayerController>();
        }
        if (other.GetComponent<Adami.HumanController>() != null)
        {
            isHumanFound = true;
            humanController = other.GetComponent<Adami.HumanController>();
        }
       
    }
    
    
    private void OnCollisionEnter(Collision other) {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<Adami.PlayerController>().Damage();
            zombieAttack.Play();
        }
        if (other.gameObject.tag == "Survivor")
        {
            other.gameObject.GetComponent<Adami.HumanController>().ZombieTransformation();
            _uiController.GetComponent<UiController>().hCount--;
            aggroList.Remove(other.gameObject);
        }
    }
    public void StartTransformation()
    {
        StartCoroutine(TransformationDelay(1));
    }
    private IEnumerator TransformationDelay(float delay)
    {
        activeAI = false;
        yield return new WaitForSeconds(delay);
        activeAI = true;
    }
    bool RandomPoint(Vector3 center, float range, out Vector3 result)
    {
        for (int i = 0; i < 30; i++)
        {
            Vector3 randomPoint = center + Random.insideUnitSphere * range;
            NavMeshHit hit;
            if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, NavMesh.AllAreas))
            {
                result = hit.position;
                return true;
            }
        }
        result = Vector3.zero;
        return false;
    }
}

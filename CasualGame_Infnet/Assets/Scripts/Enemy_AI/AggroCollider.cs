using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Adami;

public class AggroCollider : MonoBehaviour
{
   [SerializeField] private Enemy_AI enemy_AI;
   
   private void OnTriggerEnter(Collider other) 
   {
       if (other.GetComponent<Adami.PlayerController>() != null)
        {
            enemy_AI.isHumanFound = true;
            enemy_AI.playerController = other.GetComponent<Adami.PlayerController>();
            enemy_AI.aggroList.Add(other.gameObject);
        }
        if (other.GetComponent<Adami.HumanController>() != null)
        {
            enemy_AI.isHumanFound = true;
            enemy_AI.humanController = other.GetComponent<Adami.HumanController>();
            enemy_AI.aggroList.Add(other.gameObject);
        }
   }
   
   private void OnTriggerExit(Collider other) 
   {
       if (other.GetComponent<Adami.PlayerController>() != null)
        {
            enemy_AI.isHumanFound = false;
            enemy_AI.playerController = other.GetComponent<Adami.PlayerController>();
            enemy_AI.aggroList.Remove(other.gameObject);
        }
        if (other.GetComponent<Adami.HumanController>() != null)
        {
            enemy_AI.isHumanFound = false;
            enemy_AI.humanController = other.GetComponent<Adami.HumanController>();
            enemy_AI.aggroList.Remove(other.gameObject);
        }
   }
   
}

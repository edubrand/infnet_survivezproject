using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

/// COPY FROM CODE MONKEY GIVE THEM THE MONEY 
public class CutoutMaskUI : Image
{
    public override Material materialForRendering
    {
        get { 
            Material material = new Material(base.materialForRendering); 
            material.SetInt("_StencilComp", (int)CompareFunction.NotEqual); 
            return material;
        }
        
    }
}

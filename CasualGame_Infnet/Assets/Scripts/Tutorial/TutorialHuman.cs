using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialHuman : MonoBehaviour
{
    public GameObject tutorial4;
    public GameObject arrowHeli;

    void Awake()
    {
        Time.timeScale = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Adami.PlayerController>())
        {
            Time.timeScale = 0;
            tutorial4.SetActive(true);
            arrowHeli.SetActive(true);
        }
    }

    public void NotStart()
    {
        PlayerPrefs.SetInt("Tutorial", 1);
    }
}

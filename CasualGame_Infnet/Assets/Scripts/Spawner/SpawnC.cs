using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.WSA;

public class SpawnC : MonoBehaviour
{
    public GameObject[] enemies;
    public GameObject[] points;
    public int currentEnemies;
    public int maxEnemies;
    public float delay;
    public bool _spawn;

    void Start()
    {
        //InvokeRepeating("SpawnRandomPos",5,5);
        _spawn = true;

        spawner = StartCoroutine(SpawnRoutine());
    }

    // Update is called once per frame
    void Update()
    {
        if(currentEnemies>maxEnemies)
        {
            _spawn = false;
            StopCoroutine(spawner);
        }
        
    }
    public void SpawnRandomPos()
    {
        int rPos = Random.Range(0,points.Length);
        int rEnemies = Random.Range(0,enemies.Length);
        

        Instantiate(enemies[rEnemies],points[rPos].transform.position,Quaternion.identity);
        currentEnemies++;

    }

    Coroutine spawner;
    private IEnumerator SpawnRoutine()
    {
        while (_spawn)
        {
            yield return new WaitForSeconds(delay);
            SpawnRandomPos();
        }

    }

    


}

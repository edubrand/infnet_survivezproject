using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public GameObject tutorialPanel;

    public void LoadSceneName(string sceneName)
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(sceneName);
    }
    public void ApplicationQuit()
    {
        ApplicationQuit();
    }
    public void StopGame(int time)
    {
        Time.timeScale = time;
    }

    public void StartGame()
    {
        if(PlayerPrefs.HasKey("Tutorial"))
        {
            LoadSceneName("TesteIngame");
        }
        else
        {
            PlayerPrefs.SetInt("Tutorial", 1);
        }
    }
}

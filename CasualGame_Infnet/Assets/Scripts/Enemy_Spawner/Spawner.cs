using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Spawner : MonoBehaviour
{
    public bool canSpawn;
    public float spawnDelay;
    public event UnityAction<GameObject> onSpawn;
    [SerializeField] private GameObject enemyTemplate;

    public void StartSpawn(float delay)
    {
        spawnDelay = delay;
        if (spawnRoutine == null)
        {
            spawnRoutine = StartCoroutine(SpawnRoutine());
        }
    }
    public void StopSpawn()
    {
        if (spawnRoutine != null)
        {
            StopCoroutine(spawnRoutine);
        }
        spawnRoutine = null;
    }
    public void Spawn()
    {
        GameObject spawnedObj = Instantiate(enemyTemplate,transform.position,Quaternion.identity);
        onSpawn.Invoke(spawnedObj);
    }
    Coroutine spawnRoutine;
    private IEnumerator SpawnRoutine()
    {
        while (canSpawn)
        {
            yield return new WaitForSeconds(spawnDelay);
            Spawn();
        }

    }

}

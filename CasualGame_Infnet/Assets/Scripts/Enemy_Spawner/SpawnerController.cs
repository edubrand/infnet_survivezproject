using System.Collections.Generic;
using UnityEngine;

public class SpawnerController : MonoBehaviour
{
    [SerializeField] private Spawner[] spawnPoints;
    [SerializeField] private int maxEnemiesOnScreen;
    public List<GameObject> enemiesOnScreen;
    private void OnEnable() {
       for (int i =0; i<spawnPoints.Length;i++)
       {
           spawnPoints[i].onSpawn += AddEnemyToList;
       } 
    }
    private void OnDisable() {
        
    }

    private void Start() {
        for (int i =0; i<spawnPoints.Length;i++)
        {
            spawnPoints[i].Spawn();
        }
    }
    private void AddEnemyToList(GameObject enemy)
    {
        enemiesOnScreen.Add(enemy);
        if (enemiesOnScreen.Count >= maxEnemiesOnScreen)
        {
            DisableSpawns();
        }
    }
    private void RemoveEnemyFromList(GameObject enemy)
    {
        enemiesOnScreen.Remove(enemy);
    }
    private void EnableSpawns()
    {
        for (int i = 0; i< spawnPoints.Length;i++)
        {
            int randomizedDelay = Random.Range(1,5);
            spawnPoints[i].StartSpawn(randomizedDelay);
        }
    }
    private void DisableSpawns()
    {
        
    }
}

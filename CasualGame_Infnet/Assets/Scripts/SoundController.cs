using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SoundController : MonoBehaviour
{
    public AudioMixer mixer;
    public Slider masterSlider;
    public Slider musicSlider;
    public Slider sfxSlider;

    private void Start()
    {
        if(PlayerPrefs.HasKey("MasterVolume"))
        {
            float volumeMaster = PlayerPrefs.GetFloat("MasterVolume");
            mixer.SetFloat("MasterVolume", Mathf.Log10(volumeMaster) * 20);
            masterSlider.value = volumeMaster;
        }
        else
            PlayerPrefs.SetFloat("MasterVolume", 1);


        if (PlayerPrefs.HasKey("MusicsVolume"))
        {
            float volumeMusics = PlayerPrefs.GetFloat("MusicsVolume");
            mixer.SetFloat("MusicsVolume", Mathf.Log10(volumeMusics) * 20);
            musicSlider.value = volumeMusics;
        }
        else
            PlayerPrefs.SetFloat("MusicsVolume", 1);


        if (PlayerPrefs.HasKey("SFXVolume"))
        {
            float volumeSFX = PlayerPrefs.GetFloat("SFXVolume");
            mixer.SetFloat("SFXVolume", Mathf.Log10(volumeSFX) * 20);
            sfxSlider.value = volumeSFX;
        }
        else
            PlayerPrefs.SetFloat("SFXVolume", 1);
    }

    public void SetLevelMaster(float sliderValue)
    {
        mixer.SetFloat("MasterVolume", Mathf.Log10(sliderValue) * 20);
        PlayerPrefs.SetFloat("MasterVolume", sliderValue);
    }
    public void SetLevelMusics(float sliderValue)
    {
        mixer.SetFloat("MusicsVolume", Mathf.Log10(sliderValue) * 20);
        PlayerPrefs.SetFloat("MusicsVolume", sliderValue);
    }
    public void SetLevelSFX(float sliderValue)
    {
        mixer.SetFloat("SFXVolume", Mathf.Log10(sliderValue) * 20);
        PlayerPrefs.SetFloat("SFXVolume", sliderValue);
    }
}

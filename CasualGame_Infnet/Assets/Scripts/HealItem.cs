using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class HealItem : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Adami.PlayerController>())
        {
            other.gameObject.GetComponent<Adami.PlayerController>().HealInfection();
            Destroy(this.gameObject);
        }
    }
}

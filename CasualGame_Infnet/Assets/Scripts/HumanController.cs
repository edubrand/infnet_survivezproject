using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Adami
{
    public class HumanController : MonoBehaviour
    {
        public NavMeshAgent human;
        public GameObject _player;
        //public Transform Player;
        public GameObject zombieButtlerflyTemplate;
        public Animator animHuman;
        public bool follow;
        private GameObject _uiController;


        void Start()
        {
            _player = GameObject.FindGameObjectWithTag("Player");
            _uiController = GameObject.Find("UI");
            _uiController.GetComponent<UiController>().hCount++;
        }

        void Update()
        {
            if (follow)
            {
                GetRescue();
            }
        }

        public void GetRescue()
        {
            human.SetDestination(_player.transform.position);
        }
        private void OnCollisionEnter(Collision other)
        {
            if (!follow)
            {
                if (other.gameObject.tag == "Player")
                {
                    animHuman.SetTrigger("Start");
                    follow = true;
                    other.gameObject.GetComponent<Adami.PlayerController>()._humans.Add(gameObject);
                    other.gameObject.GetComponent<Adami.PlayerController>().playerHP++;
                    other.gameObject.GetComponent<Adami.PlayerController>().UpdateUI();
                }
            }
        }

        public void ZombieTransformation()
        {
            follow = false;
            GameObject clone = Instantiate(zombieButtlerflyTemplate, transform.position, Quaternion.identity);
            clone.GetComponent<Enemy_AI>().StartTransformation();
            Destroy(this.gameObject);
        }
    }
}
